// SPDX-License-Identifier: GPL-3.0-only
use std::collections::HashMap;
use std::fs;

use clap::Parser;
use estahr::strings::levenshtein_ascii;
use nom_bibtex::Bibliography;
use nom_bibtex::Bibtex;

extern crate nom_bibtex;

struct TitleComparisonConfig {
    maximum_same_distance: usize,
    removal_list: Vec<String>,
    skip_arxiv: bool,
}

#[derive(Debug)]
struct AnalyzedBibliography<'a> {
    unique_records: Vec<&'a Bibliography>,
    repeat_clusters: Vec<Vec<&'a Bibliography>>,
}

#[derive(clap::Parser)]
#[command(about = "Compare 2 bibtex files to find record that are not present in the other ond")]
struct CmdOptions {
    #[arg(long = "file1", short = '1', help = "First bibtex file to process")]
    bibtex1_path: String,
    #[arg(long = "file2", short = '2', help = "Second bibtex file to process")]
    bibtex2_path: String,
    #[arg(long = "skip-arxiv", short = 's', help = "Skip arxiv entries")]
    skip_arxiv: bool,
}

fn main() {
    let args = CmdOptions::parse();

    let config = TitleComparisonConfig {
        maximum_same_distance: 20,
        removal_list: [
            "<sub>",
            "</sub>",
            "<sup>",
            "</sup>",
            "<inf>",
            "</inf>",
            "\\mathbf",
            "\\sqrt",
            "\\ensuremath",
            "\\textrm",
            "\rm",
            "$",
            "\\textendash{}",
            "{",
            "}",
        ]
        .map(|s| s.to_string())
        .to_vec(),
        skip_arxiv: args.skip_arxiv,
    };

    let bibtex1 = read_bib(&args.bibtex1_path);
    let analyzed_bib1 = analyze_for_repeats(&bibtex1, &config);

    let bibtex2 = read_bib(&args.bibtex2_path);
    let analyzed_bib2 = analyze_for_repeats(&bibtex2, &config);

    println!("First file stats");
    print_analysis_result(&analyzed_bib1);
    println!("Second file stats +++++++++++");
    print_analysis_result(&analyzed_bib2);

    println!(
        "*** Present in {} but not in {}",
        &args.bibtex1_path, &args.bibtex2_path
    );
    print_not_present_items(
        &analyzed_bib1.unique_records,
        &analyzed_bib2.unique_records,
        &config,
    );

    println!(
        "*** Present in {} but not in {}",
        &args.bibtex2_path, &args.bibtex1_path
    );
    print_not_present_items(
        &analyzed_bib2.unique_records,
        &analyzed_bib1.unique_records,
        &config,
    );
}

fn print_not_present_items(
    unique_records_1: &Vec<&Bibliography>,
    unique_records_2: &Vec<&Bibliography>,
    config: &TitleComparisonConfig,
) {
    let mut result: Vec<&Bibliography> = unique_records_1
        .iter()
        .filter(|bib| {
            let my_title = cleanup_entry_title(bib, &config);
            !unique_records_2.iter().any(|bib2| {
                let other_title = cleanup_entry_title(bib2, &config);
                compare_entries(&bib, &my_title, &bib2, &other_title, config)
            })
        })
        .map(|x| x.to_owned())
        .collect();
    result.sort_by_key(|bib| bib.tags()["title"].as_str());
    result
        .iter()
        .for_each(|bib| println!("{}", bib.tags()["title"]));
}

fn print_analysis_result(analyzed_bib: &AnalyzedBibliography<'_>) {
    println!("Unique records {}:", analyzed_bib.unique_records.len());
    analyzed_bib
        .unique_records
        .iter()
        .for_each(|r| println!("{}", r.tags()["title"]));
    analyzed_bib.repeat_clusters.iter().for_each(|c| {
        println!("CLUSTER----------------");
        c.iter()
            .for_each(|entry| println!("{}", entry.tags()["title"]));
    });
}

fn read_bib(name: &str) -> Bibtex {
    let bibtex_contents1 = fs::read_to_string(name).unwrap();
    Bibtex::parse(&bibtex_contents1).unwrap()
}

fn cleanup_title(title: &str, config: &TitleComparisonConfig) -> String {
    config
        .removal_list
        .iter()
        .fold(title.to_string(), |title, rem| title.replace(rem, ""))
        .to_ascii_lowercase()
}

fn cleanup_entry_title(entry: &Bibliography, config: &TitleComparisonConfig) -> String {
    cleanup_title(entry.tags()["title"].as_str(), &config)
}

fn are_cleaned_titles_same(title1: &str, title2: &str, config: &TitleComparisonConfig) -> bool {
    levenshtein_ascii(title1, title2) <= config.maximum_same_distance
}

fn is_from_arxiv(entry: &Bibliography) -> bool {
    let arxiv: String = "arXiv".to_string();
    entry.tags().get("archiveprefix").eq(&Some(&arxiv))
        || entry.tags().get("journal").eq(&Some(&arxiv))
        || entry
            .tags()
            .get("doi")
            .map(|str| str.contains("arXiv"))
            .unwrap_or(false)
}

fn compare_entries(
    entry1: &Bibliography,
    normalized_title1: &str,
    entry2: &Bibliography,
    normalized_title_2: &str,
    config: &TitleComparisonConfig,
) -> bool {
    match (entry1.tags().get("doi"), entry2.tags().get("doit")) {
        (Some(doi1), Some(doi2)) => doi1.eq(doi2),
        _ => are_cleaned_titles_same(normalized_title1, normalized_title_2, config),
    }
}

fn analyze_for_repeats<'a>(
    bibtex: &'a Bibtex,
    config: &TitleComparisonConfig,
) -> AnalyzedBibliography<'a> {
    let bib_indices_closure = || {
        bibtex
            .bibliographies()
            .iter()
            .zip(0..bibtex.bibliographies().len())
            .filter(|(entry, _)| {
                if config.skip_arxiv {
                    !is_from_arxiv(entry)
                } else {
                    true
                }
            })
    };

    fn make_map_key(pair: (&Bibliography, usize)) -> String {
        format!("{}-{}", pair.1, pair.0.citation_key())
    }

    let mut map: HashMap<String, &'a Bibliography> = HashMap::new();

    bib_indices_closure().for_each(|(be, idx)| {
        map.insert(make_map_key((be, idx)), be);
    });

    let mut ret = AnalyzedBibliography {
        unique_records: Vec::new(),
        repeat_clusters: Vec::new(),
    };

    bib_indices_closure()
        .map(make_map_key)
        .for_each(|citation_key| match map.get(&citation_key) {
            None => {}
            Some(curr_record) => {
                ret.unique_records.push(curr_record);
                let mut cluster: Vec<&'a Bibliography> =
                    [map.get(&citation_key).unwrap().to_owned()].to_vec();
                let curr_title = cleanup_entry_title(&curr_record, config);
                let rec = curr_record.to_owned();
                map.remove(&citation_key);
                let to_remove: Vec<String> = map
                    .iter()
                    .filter(|entry| {
                        let title = cleanup_entry_title(&entry.1, config);
                        compare_entries(rec, &curr_title, &entry.1, &title, config)
                    })
                    .map(|e| e.0.to_string())
                    .collect();
                to_remove.iter().for_each(|s| {
                    let val = map.remove(s.as_str()).unwrap();
                    cluster.push(val);
                });
                ret.repeat_clusters.push(cluster)
            }
        });

    ret
}
