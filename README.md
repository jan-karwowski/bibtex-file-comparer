# bibtex-file-comparer

The sole goal of this program is to compare if two bibliographic databases contain the same entries for an author. Usage: download author's works from each database in bibtex format and run the program on those files.

```
bibtex-file-comparer -1 export-from-db1.bib -2 export-from-db2-bib

## Why?

The comparison between automatically populated databases by hand is troublesome. Some works are duplicated with slight differences in title (like punctuation marks on different ascii degradation of mathematical formulas). The titles between database can also be slightly different because of such factors. In fields like physics where people co-author a lot of papers per year doing it by hand requires significant time resources.

The program detects duplicates within each database separately (by removing artifacts from the titles and matching titles using Levensthein distance <= 20) and then compares entries in both databases after removing duplicates. The result is printed to stdout. 
